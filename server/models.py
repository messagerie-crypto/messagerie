import uuid
from sqlalchemy import Column, String, Text
from database import Base


class Channel(Base):
    __tablename__ = "channels"
    id = Column(String(36), primary_key=True, index=True)
    name = Column(String(255), index=True)
    encrypted_token = Column(Text, index=True)
