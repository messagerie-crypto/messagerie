from pydantic import BaseModel


class ChannelCreate(BaseModel):
    id: str
    name: str
    encrypted_token: str


class ChannelResponse(BaseModel):
    id: str
    name: str
    encrypted_token: str
