from fastapi import FastAPI, Depends, HTTPException, WebSocket
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session
from starlette.websockets import WebSocketDisconnect

from database import Base, engine, get_db
from models import Channel
from schemas import ChannelResponse, ChannelCreate

app = FastAPI()
active_websockets = {}

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/channels/", response_model=ChannelResponse)
def create_channel(channel: ChannelCreate, db: Session = Depends(get_db)):
    db_channel = Channel(id=channel.id, name=channel.name, encrypted_token=channel.encrypted_token)
    db.add(db_channel)
    db.commit()
    db.refresh(db_channel)
    print(db_channel)
    return db_channel


@app.get("/channels/{channel_id}", response_model=ChannelResponse)
def get_channel(channel_id: str, db: Session = Depends(get_db)):
    db_channel = db.query(Channel).get(channel_id)

    if db_channel is None:
        raise HTTPException(status_code=404)

    return db_channel


@app.websocket("/ws/{channel_id}")
async def websocket_endpoint(websocket: WebSocket, channel_id: str):
    await websocket.accept()

    if channel_id not in active_websockets:
        active_websockets[channel_id] = []

    active_websockets[channel_id].append(websocket)

    try:
        while True:
            data = await websocket.receive_text()

            for ws in active_websockets[channel_id]:
                await ws.send_text(data)

    except WebSocketDisconnect:
        active_websockets[channel_id].remove(websocket)
        if not active_websockets[channel_id]:
            del active_websockets[channel_id]



@app.on_event("startup")
def startup_event():
    Base.metadata.create_all(bind=engine)
