async function generateKeyFromPassword(password, salt) {
    const encoder = new TextEncoder();
    const passwordData = encoder.encode(password);
    const saltData = encoder.encode(salt);

    const keyMaterial = await window.crypto.subtle.importKey(
        'raw',
        passwordData,
        {
            name: 'PBKDF2',
        },
        false,
        ['deriveBits', 'deriveKey']
    );

    const derivedBits = await window.crypto.subtle.deriveBits(
        {
            name: 'PBKDF2',
            salt: saltData,
            iterations: 10000,
            hash: 'SHA-256',
        },
        keyMaterial,
        256
    );

    return window.crypto.subtle.importKey(
        'raw',
        derivedBits,
        {
            name: 'AES-GCM',
        },
        false,
        ['encrypt', 'decrypt']
    );
}


async function encryptData(data, key) {
    const encoder = new TextEncoder();
    const encodedData = encoder.encode(data);

    const iv = window.crypto.getRandomValues(new Uint8Array(12));

    const encryptedData = await window.crypto.subtle.encrypt(
        {
            name: 'AES-GCM',
            iv,
        },
        key,
        encodedData
    );

    return {
        encryptedMessage: new Uint8Array(encryptedData),
        iv,
    };
}

async function decryptData(encryptedMessage, key, iv) {
    const decryptedData = await window.crypto.subtle.decrypt(
        {
            name: 'AES-GCM',
            iv,
        },
        key,
        encryptedMessage
    );

    const decoder = new TextDecoder();
    return decoder.decode(decryptedData);
}

async function encryptDataWithPassword(data, password, salt) {
    const key = await generateKeyFromPassword(password, salt);
    return encryptedDataToString(await encryptData(data, key));
}

async function decryptDataWithPassword(encryptedDataString, password, salt) {
    const key = await generateKeyFromPassword(password, salt);
    const { encryptedMessage, iv } = encryptedDataFromString(encryptedDataString);
    return decryptData(encryptedMessage, key, iv);

}

async function generateRandomToken(length) {
    const crypto = window.crypto

    if (!crypto) {
        throw new Error("La génération de token aléatoire n'est pas prise en charge dans ce navigateur.");
    }

    const tokenArray = new Uint8Array(length);
    crypto.getRandomValues(tokenArray);

    return Array.from(tokenArray)
        .map(byte => byte.toString(16).padStart(2, '0'))
        .join('');
}

function encryptedDataToString(encryptedData) {
    const encryptedDataBase64 = arrayBufferToBase64(encryptedData.encryptedMessage);
    const ivBase64 = arrayBufferToBase64(encryptedData.iv);

    return JSON.stringify({ encryptedMessage: encryptedDataBase64, iv: ivBase64 });
}

function encryptedDataFromString(str) {
    const obj = JSON.parse(str);
    const encryptedMessage = base64ToArrayBuffer(obj.encryptedMessage);
    const iv = base64ToArrayBuffer(obj.iv);

    return { encryptedMessage, iv };
}

function base64ToArrayBuffer(base64) {
    const binaryString = atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
}


function arrayBufferToBase64(buffer) {
    return btoa(String.fromCharCode.apply(null, new Uint8Array(buffer)));
}


export { generateKeyFromPassword, encryptData, decryptData, generateRandomToken, encryptedDataToString, encryptedDataFromString, decryptDataWithPassword, encryptDataWithPassword };