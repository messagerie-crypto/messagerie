import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import ChannelView from '../views/ChannelView.vue';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomeView,
        meta: {
            title: 'Créez un salon'
        }
    },
    {
        path: '/channel/:uuid',
        name: 'Channel',
        component: ChannelView,
        props: true
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;